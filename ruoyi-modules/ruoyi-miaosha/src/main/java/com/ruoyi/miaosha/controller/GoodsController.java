package com.ruoyi.miaosha.controller;

import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.miaosha.domain.MiaoShaOrder;
import com.ruoyi.miaosha.domain.vo.GoodsVo;
import com.ruoyi.miaosha.result.Result;
import com.ruoyi.miaosha.service.GoodsService;
import com.ruoyi.miaosha.utils.OssUtil;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author: 孙佳婧
 * @Description:
 * @data 2023/9/28 20:38
 * @fileName GoodsController
 */
/*
* 666666666666666
* */
@RestController
@RequestMapping("/goods")
@Slf4j
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private HttpServletRequest request;
/*
* 商品列表页面
* */
@PostMapping("/goodsList")
    public Result<List<GoodsVo>> goodsList(){

    log.info("功能名称:商品列表,请求URI:[{}],请求方式:[{}],请求参数:[{}]",request.getRequestURI(),request.getMethod(),null);

    List<GoodsVo> list=goodsService.goodsList();

    Result<List<GoodsVo>> success = Result.success(list);

    log.info("功能名称:商品列表,请求URI:[{}],请求方式:[{}],响应参数:[{}]",request.getRequestURI(),request.getMethod(),null);

    return success;
}
/*
* jkhjjhkjhkjlkj 66666655445444
* */

/*
* 商品详情页面  根据ID查看
* */
@PostMapping("/findById/{goodsId}")
    public Result findById(@PathVariable Integer goodsId){

    log.info("功能名称:商品详情页面,请求URI:[{}],请求方式:[{}],请求参数:[{}]",request.getRequestURI(),request.getMethod(),goodsId);

    GoodsVo goodsVo=goodsService.findById(goodsId);

    Result<GoodsVo> success = Result.success(goodsVo);

    log.info("功能名称:商品详情页面,请求URI:[{}],请求方式:[{}],响应参数:[{}]",request.getRequestURI(),request.getMethod(),goodsId);

    return success;
}

/*
* 根据ID秒杀指定商品
* */
    @PostMapping("/ToDetail/{goodsId}")
    public Result<GoodsVo> ToDetail(@PathVariable Integer goodsId){

        log.info("功能名称:根据ID秒杀指定商品,请求URI:[{}],请求方式:[{}],请求参数:[{}]",request.getRequestURI(),request.getMethod(),goodsId);

        Result<GoodsVo> result=goodsService.ToDetail(goodsId);

        log.info("功能名称:根据ID秒杀指定商品,请求URI:[{}],请求方式:[{}],响应参数:[{}]",request.getRequestURI(),request.getMethod(),goodsId);

       return result;
    }



    /*
    * 秒杀商品 库存减少
    * */
    @PostMapping("/reduce/{goodsId}")
    public Result reduce(@PathVariable Integer goodsId){

        Result result=goodsService.reduce(goodsId);

        return result;
    }

    /*
    * OSS图片上传
    * */
    @PostMapping("/OssUpLoad")
    public String OssUpLoad(@RequestParam("file") MultipartFile multipartFile){
        return OssUtil.uploadMultipartFile(multipartFile);
    }

    @RequestMapping("/miaosha/{goodsId}")
    public Result miaosha(@PathVariable Integer goodsId){
        return Result.success(goodsService.miaosha(goodsId));
    }

//    @PostMapping("/getMiaoshaOrderByUserIdAndGoodsId/{goodsId}")
//    public Result getMiaoshaOrderByUserIdAndGoodsId(@PathVariable Integer goodsId){
//
//        log.info("功能名称:判断是否已经秒杀到了,请求URI:[{}],请求方式:[{}],请求参数:[{}]",request.getRequestURI(),request.getMethod(),goodsId);
//
//        MiaoShaOrder miaoshaOrder=goodsService.getMiaoshaOrderByUserIdAndGoodsId(goodsId);
//
//        Result<MiaoShaOrder> success = Result.success(goodsVo);
//
//        log.info("功能名称:判断是否已经秒杀到了,请求URI:[{}],请求方式:[{}],响应参数:[{}]",request.getRequestURI(),request.getMethod(),goodsId);
//
//        return success;
//    }
}
