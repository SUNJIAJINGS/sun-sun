package com.ruoyi.miaosha;

import java.util.*;

/**
 * @Author: 孙佳婧
 * @Description:
 * @data 2023/10/8 13:39
 * @fileName Test
 */
public class Test {



    public static class RandomNumberAssignment {
        public static void main(String[] args) {
            List<String> names = new ArrayList<>();
            names.add("张羿涵");
            names.add("李帆");
            names.add("刘杰");
            names.add("陈永霄");
            names.add("黄朝辉");
            names.add("张凯文");
            names.add("靳飞龙");
            names.add("吴珍珠");
            names.add("赵东旭");
            names.add("孙佳婕");
            names.add("李梦波");
            names.add("程强华");
            names.add("马洪凯");
            names.add("孙佳婧");
            names.add("杨燕彤");
            names.add("王利军");
            names.add("张家慧");
            names.add("汤宇航");
            names.add("常思悦");
            names.add("陈益楠");
            names.add("董腾允");
            names.add("余满");
            names.add("成威");
            names.add("常瑞");
            names.add("刘炎鑫");
            names.add("刘晓慧");
            names.add("马金磊");
            names.add("秦凌晨");

            List<Integer> randomNumbers = new ArrayList<>();

            Random random = new Random();
            Set<Integer> usedNumbers = new HashSet<>();

            for (String name : names) {
                int randomNumber;
                do {
                    randomNumber = random.nextInt(10);
                } while (usedNumbers.contains(randomNumber));

                usedNumbers.add(randomNumber);
                randomNumbers.add(randomNumber);
            }

            for (int i = 0; i < names.size(); i++) {
                System.out.println(names.get(i) + " " + randomNumbers.get(i));
            }
        }
    }
}
