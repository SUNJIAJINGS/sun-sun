package com.ruoyi.miaosha.service.lmpl;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.miaosha.domain.MiaoShaGoods;
import com.ruoyi.miaosha.domain.MiaoShaOrder;
import com.ruoyi.miaosha.domain.OrderInfo;
import com.ruoyi.miaosha.domain.vo.GoodsVo;
import com.ruoyi.miaosha.mapper.GoodsMapper;
import com.ruoyi.miaosha.result.Result;
import com.ruoyi.miaosha.service.GoodsService;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Author: 孙佳婧
 * @Description:
 * @data 2023/10/1 14:33
 * @fileName GoodsServiceImpl
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private GoodsService goodsService;

    @Override
    public List<GoodsVo> goodsList() {
        return goodsMapper.goodsList();
    }

    @Override
    public GoodsVo findById(Integer goodsId) {
        return goodsMapper.findById(goodsId);
    }

@Override
public Result<GoodsVo> ToDetail(Integer goodsId) {

GoodsVo goods=goodsMapper.findById(goodsId);

long startTime=goods.getStartDate().getTime();

long endTime=goods.getEndDate().getTime();

long now=System.currentTimeMillis();

int miaoshaStatus=0;
int remainSeconds=0;

if (now<startTime){//秒杀还未开始 倒计时...
  miaoshaStatus=0;
  remainSeconds=(int)((startTime-now)/1000);
}else if (now>endTime){//秒杀已经结束
    miaoshaStatus=2;
    remainSeconds=-1;
}else {//秒杀进行中...
    miaoshaStatus=1;
    remainSeconds=0;
}
    System.out.printf(String.valueOf(remainSeconds));
    System.out.printf(String.valueOf(miaoshaStatus));
        goods.setSeckillStatus(miaoshaStatus);
        goods.setRemainSeconds(remainSeconds);
        return Result.success(goods);
    }

    @Override
    public Result reduce(Integer goodsId) {

        Integer i=goodsMapper.reduce(goodsId);

        if (i>0){

            MiaoShaOrder miaoShaOrder = new MiaoShaOrder();

            miaoShaOrder.setUserId(1);

            Integer id = miaoShaOrder.getId();

            miaoShaOrder.setOrderId(id);

            miaoShaOrder.setGoodsId(goodsId);

            goodsMapper.insertOrder(miaoShaOrder);

            goodsMapper.reduceStock(goodsId);

            return Result.success("库存减少成功");

        }else {

            return Result.error("库存减少失败");
        }
    }

    @Override
    public Result miaosha(Integer goodsId) {

        LoginUser user= SecurityUtils.getLoginUser();

        System.out.printf("用户信息"+user);

        if (user==null){
            throw new RuntimeException("用户信息异常");
        }

        //判断商品库存
        GoodsVo goodsVo=goodsMapper.findById(goodsId);

        //获取秒杀的库存（与实际库存不匹配）
        Integer stockCount = goodsVo.getStockCount();

        if (stockCount<0){
            throw new RuntimeException("秒杀数量已达上限");
        }

        //判断是否已经秒杀到了（防止一个人秒杀多个商品）
        MiaoShaOrder order=goodsService.getMiaoshaOrderByUserIdAndGoodsId(user.getUserid(),goodsId);

        if (order!=null){
            throw new RuntimeException("不能重复秒杀!");
        }

        OrderInfo orderInfo=goodsService.miaoshaVo(user,goodsVo);

        return null;
    }

    @Override
    public MiaoShaOrder getMiaoshaOrderByUserIdAndGoodsId(Long userId, Integer goodsId) {
        return goodsMapper.getMiaoshaOrderByUserIdAndGoodsId(userId,goodsId);
    }

    @Override
    public OrderInfo miaoshaVo(LoginUser user, GoodsVo goodsVo) {

        goodsService.reduce(goodsVo.getGoodsId());

        OrderInfo orderInfo=goodsService.createOrder(user,goodsVo);

        return orderInfo;
    }

    @Override
    public OrderInfo createOrder(LoginUser user, GoodsVo goodsVo) {
        return null;
    }


}


