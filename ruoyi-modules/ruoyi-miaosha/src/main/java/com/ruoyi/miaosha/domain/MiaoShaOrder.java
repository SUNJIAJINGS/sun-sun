package com.ruoyi.miaosha.domain;

import lombok.Data;

/**
 * @Author: 孙佳婧
 * @Description:
 * @data 2023/9/28 20:04
 * @fileName MiaoShaOrder
 */
@Data
public class MiaoShaOrder {


    private Integer id;
    private Integer userId;
    private Integer orderId;
    private Integer goodsId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }
}
