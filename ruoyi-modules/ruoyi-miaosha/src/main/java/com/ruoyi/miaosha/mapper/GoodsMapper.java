package com.ruoyi.miaosha.mapper;

import com.ruoyi.miaosha.domain.MiaoShaOrder;
import com.ruoyi.miaosha.domain.vo.GoodsVo;
import com.ruoyi.miaosha.result.Result;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: 孙佳婧
 * @Description:
 * @data 2023/10/1 14:34
 * @fileName GoodsMapper
 */
@Mapper
public interface GoodsMapper {
    List<GoodsVo> goodsList();

    GoodsVo findById(Integer goodsId);

    Integer reduce(Integer goodsId);

    void insertOrder(MiaoShaOrder miaoShaOrder);

    MiaoShaOrder findOrder(Integer goodsId);

    void reduceStock(Integer goodsId);

    MiaoShaOrder getMiaoshaOrderByUserIdAndGoodsId(@Param("userId") Long userId, @Param("goodsId") Integer goodsId);
}
