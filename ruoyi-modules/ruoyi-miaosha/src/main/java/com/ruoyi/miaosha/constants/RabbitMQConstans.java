package com.ruoyi.miaosha.constants;

/**
 * @Author: 孙佳婧
 * @Description: 专高六
 * @data 2023/7/6 15:48
 * @fileName RabbitMQConstans
 */

public class RabbitMQConstans {

    public static final String SEND_MESSAGE_QUEUE_NAME="send_message";
}
