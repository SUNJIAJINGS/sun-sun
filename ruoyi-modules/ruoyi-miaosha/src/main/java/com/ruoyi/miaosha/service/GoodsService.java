package com.ruoyi.miaosha.service;

import com.ruoyi.miaosha.domain.MiaoShaOrder;
import com.ruoyi.miaosha.domain.OrderInfo;
import com.ruoyi.miaosha.domain.vo.GoodsVo;
import com.ruoyi.miaosha.result.Result;
import com.ruoyi.system.api.model.LoginUser;

import java.util.List;

/**
 * @Author: 孙佳婧
 * @Description:
 * @data 2023/10/1 14:33
 * @fileName GoodsService
 */
public interface GoodsService {
    List<GoodsVo> goodsList();

    GoodsVo findById(Integer goodsId);

    Result ToDetail(Integer goodsId);

    Result reduce(Integer goodsId);


    Result miaosha(Integer goodsId);

    MiaoShaOrder getMiaoshaOrderByUserIdAndGoodsId(Long userId, Integer goodsId);


    OrderInfo miaoshaVo(LoginUser user, GoodsVo goodsVo);

    OrderInfo createOrder(LoginUser user, GoodsVo goodsVo);
}
